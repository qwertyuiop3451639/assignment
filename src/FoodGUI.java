import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton otoyalunchButton;
    private JButton nanbanButton;
    private JButton kurozuanButton;
    private JTextArea orderedItemsList;
    private JButton maboButton;
    private JButton karaageButton;
    private JButton kaasannButton;
    private JButton checkOutButton;
    private JTextArea totalArea;
    private JLabel iconLabel;
    private JTextField nanbanTextField;
    private JTextField kurozuTextField;
    private JTextField otoyalunchTextField;
    private JTextField kaasannTextField;
    private JTextField karaageTextField;
    private JButton takeOutButton;
    private JTextField maboTextField;


    void order(String food, int val){
            int confirmation1 = JOptionPane.showConfirmDialog(null,
                    food+"　を注文しますか?\n",
                    "Order Confilmation1",
                    JOptionPane.YES_NO_OPTION);
            if(confirmation1==0){
                int confirmation2 = JOptionPane.showConfirmDialog(null,
                        food+"　をテイクアウトしますか?",
                        "Order Confilmation2",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation2==0){
                    JOptionPane.showMessageDialog(null, food+"　の注文を受け付けました。(テイクアウト)");
                    String currentText = orderedItemsList.getText();
                    orderedItemsList.setText(currentText+food+"(テ)\n");
                    int currentTotal = Integer.parseInt(totalArea.getText());
                    totalArea.setText(String.valueOf(currentTotal+val*54/55));
                } else if (confirmation2==1) {
                    JOptionPane.showMessageDialog(null, food+"　の注文を受け付けました。(店内)");
                    String currentText = orderedItemsList.getText();
                    orderedItemsList.setText(currentText+food+"(店)\n");
                    int currentTotal = Integer.parseInt(totalArea.getText());
                    totalArea.setText(String.valueOf(currentTotal+val));
                }

            }
    }
    public FoodGUI() {
        int count=0;
        JTextArea tA1=new JTextArea("orderedItemsList");
        nanbanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("大戸屋風チキン南蛮定食",980);
            }
        });
        nanbanButton.setIcon(new ImageIcon(
                this.getClass().getResource("nanban.jpg")
        ));
        kurozuanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { order("すけそう鱈と野菜の黒酢あん定食",1000);
            }
        });
        kurozuanButton.setIcon(new ImageIcon(
                this.getClass().getResource("sukesoudara.jpg")
        ));
        otoyalunchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("大戸屋ランチ定食",890);
            }
        });
        otoyalunchButton.setIcon(new ImageIcon(
                this.getClass().getResource("ootoyalunch.jpg")
        ));
        kaasannButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { order("チキンかあさん煮定食",950);}
        });
        kaasannButton.setIcon(new ImageIcon(
                this.getClass().getResource("kaasannni.jpg")
        ));
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { order("甘からだれの鳥唐揚げ定食",940);}
        });
        karaageButton.setIcon(new ImageIcon(
                this.getClass().getResource("karaage.jpg")
        ));
        maboButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { order("ピリ辛 本格麻婆豆腐定食",940);}
        });
        maboButton.setIcon(new ImageIcon(
                this.getClass().getResource("mabo.jpg")
        ));
        iconLabel.setIcon(new ImageIcon(
                this.getClass().getResource("logo.png")
        ));
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "注文を終えますか?",
                        "CheckOut Confilmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation==0){
                    String currentTotal = totalArea.getText();
                    JOptionPane.showMessageDialog(null, currentTotal+"円になります。\nありがとうございました。");
                    orderedItemsList.setText("");
                    totalArea.setText(String.valueOf(0));
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}